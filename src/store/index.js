import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    data: [],
  },
  getters: {
    getAllData: state => state.data,
  },
  actions: {
    async getDataFromDB({ commit }) {
      try {
        const response = await axios.get(`/api/data`);
        console.log(response.data);
        commit("setDataFromDB", response.data);
      } catch (error) {
        console.error(error);
      }
    },
    async deleteCardFromDB({ commit }, cardID) {
      try {
        await axios.delete(`/api/data/${Number(cardID)}`);
        commit("deleteCard", { id: Number(cardID) });
      } catch (error) {
        console.error(error);
      }
    },
    async updateCardFromDB({ commit }, card) {
      const { id } = card;
      try {
        const response = await axios.put(`/api/data/${Number(id)}`, card);
        commit("updateCard", response.data);
      } catch (error) {
        console.error(error);
      }
    },
  },
  mutations: {
    setDataFromDB: (state, payload) => (state.data = payload),
    deleteCard: (state, payload) => (state.data = state.data.filter(card => card.id !== payload.id)),
    updateCard: (state, payload) =>
      state.data.forEach(card => {
        if (card.id === payload.id) card = payload;
      }),
  },
});
